import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  item: Observable<any>;
  courseDoc: AngularFirestoreDocument<any>;

  itemsCollection: AngularFirestoreCollection<any[]>;
  items: Observable<any[]>;
  snapshot: any;
  newContent: string;
  
  public roomForm: FormGroup;

  constructor(private router: Router, public formBuilder: FormBuilder, private db: AngularFirestore) {}

  ngOnInit() {
	this.getitems('asc');
    this.roomForm = this.formBuilder.group({
      roomName: ['', Validators.compose([Validators.required])],
    });
  }

    getitems(orderBy) {
    this.itemsCollection = this.db.collection('tasks', ref => {
      return ref.orderBy('orden', orderBy);
    });

    this.items = this.itemsCollection.valueChanges();
    this.snapshot = this.itemsCollection.snapshotChanges()
      .map(arr => {
        return arr.map(snap => {
          const data = snap.payload.doc.data();
          const id = snap.payload.doc.id;
          return { id, ...data };
        });
      });
  }
  
   public goToVideoCall(idCall) {
    this.router.navigate(['/', idCall]);
    }
	

}
